# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Kishore G <kishore96@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:13+0000\n"
"PO-Revision-Date: 2022-06-28 21:22+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "பொதுவானவை"

#: contents/ui/main.qml:243
#, kde-format
msgctxt "@action"
msgid "Paste"
msgstr "ஒட்டு"

#: contents/ui/main.qml:252 contents/ui/main.qml:293
#, kde-format
msgid "Share"
msgstr "பகிர்"

#: contents/ui/main.qml:253 contents/ui/main.qml:294
#, kde-format
msgid "Drop text or an image onto me to upload it to an online service."
msgstr "உரையையோ படத்தையோ ஓர் இணையச்சேவைக்கு பதிவேற்ற என்மேல் அதை இழுத்துப்போடுங்கள்."

#: contents/ui/main.qml:294
#, kde-format
msgid "Upload %1 to an online service"
msgstr "%1 என்பதை இணையதளத்திற்கு பதிவேற்று"

#: contents/ui/main.qml:307
#, kde-format
msgid "Sending…"
msgstr "அனுப்புகிறது…"

#: contents/ui/main.qml:308
#, kde-format
msgid "Please wait"
msgstr "காத்திருக்கவும்"

#: contents/ui/main.qml:315
#, kde-format
msgid "Successfully uploaded"
msgstr "வெற்றிகரமாக பதிவேற்றப்பட்டுள்ளது"

#: contents/ui/main.qml:316
#, kde-format
msgid "<a href='%1'>%1</a>"
msgstr "<a href='%1'>%1</a>"

#: contents/ui/main.qml:323
#, kde-format
msgid "Error during upload."
msgstr "பதிவேற்றத்தின்போது சிக்கல்"

#: contents/ui/main.qml:324
#, kde-format
msgid "Please, try again."
msgstr "தயவுசெய்து மீண்டும் முயற்சியுங்கள்"

#: contents/ui/settingsGeneral.qml:21
#, kde-format
msgctxt "@label:spinbox"
msgid "History size:"
msgstr "வரலாற்றின் அளவு:"

#: contents/ui/settingsGeneral.qml:31
#, kde-format
msgctxt "@option:check"
msgid "Copy automatically:"
msgstr "தானாக நகலெடு:"

#: contents/ui/ShareDialog.qml:30
#, kde-format
msgid "Shares for '%1'"
msgstr "'%1' என்பதற்கான பகிர்வுகள்"

#: contents/ui/ShowUrlDialog.qml:42
#, kde-format
msgid "The URL was just shared"
msgstr "முகவரி பகிரப்பட்டுள்ளது"

#: contents/ui/ShowUrlDialog.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Don't show this dialog, copy automatically."
msgstr "இச்சாளரத்தை காட்டாமல் தானாக நகலெடு"

#: contents/ui/ShowUrlDialog.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Close"
msgstr "மூடு"
