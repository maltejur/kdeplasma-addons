# Malayalam translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:13+0000\n"
"PO-Revision-Date: 2018-08-16 09:14+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:30
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum columns:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:30
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum rows:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:44
#, kde-format
msgctxt "@title:group"
msgid "Appearance:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Show launcher names"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "Enable popup"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Title:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:69
#, kde-format
msgctxt "@option:check"
msgid "Show:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@info:placeholder"
msgid "Custom title"
msgstr ""

#: package/contents/ui/IconItem.qml:175
#, kde-format
msgid "Launch %1"
msgstr ""

#: package/contents/ui/IconItem.qml:259
#, kde-format
msgctxt "@action:inmenu"
msgid "Add Launcher…"
msgstr ""

#: package/contents/ui/IconItem.qml:265
#, kde-format
msgctxt "@action:inmenu"
msgid "Edit Launcher…"
msgstr ""

#: package/contents/ui/IconItem.qml:271
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove Launcher"
msgstr ""

#: package/contents/ui/main.qml:145
#, kde-format
msgid "Quicklaunch"
msgstr ""

#: package/contents/ui/main.qml:146
#, kde-format
msgctxt "@info"
msgid "Add launchers by Drag and Drop or by using the context menu."
msgstr ""

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Hide icons"
msgstr ""

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Show hidden icons"
msgstr ""

#: package/contents/ui/main.qml:291
#, kde-format
msgctxt "@action"
msgid "Add Launcher…"
msgstr ""
