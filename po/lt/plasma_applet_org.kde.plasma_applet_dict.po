# Lithuanian translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-11 02:11+0000\n"
"PO-Revision-Date: 2022-11-17 23:36+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Dictionaries"
msgstr "Žodynai"

#: package/contents/ui/AvailableDictSheet.qml:33
#, kde-format
msgid "Add More Dictionaries"
msgstr "Pridėti daugiau žodynų"

#: package/contents/ui/ConfigDictionaries.qml:89
#, kde-format
msgid "Unable to load dictionary list"
msgstr "Nepavyko įkelti žodynų sąrašo"

#: package/contents/ui/ConfigDictionaries.qml:90
#: package/contents/ui/main.qml:155
#, kde-format
msgctxt "%2 human-readable error string"
msgid "Error code: %1 (%2)"
msgstr "Klaidos kodas: %1 (%2)"

#: package/contents/ui/ConfigDictionaries.qml:101
#, kde-format
msgid "No dictionaries"
msgstr "Nėra žodynų"

#: package/contents/ui/ConfigDictionaries.qml:112
#, kde-format
msgid "Add More…"
msgstr "Pridėti daugiau…"

#: package/contents/ui/DictItemDelegate.qml:51
#, kde-format
msgid "Delete"
msgstr "Ištrinti"

#: package/contents/ui/main.qml:46
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter word to define here…"
msgstr "Čia įveskite norimą apibrėžti žodį…"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Unable to load definition"
msgstr "Nepavyko įkelti apibrėžimo"

#~ msgid "Looking up definition…"
#~ msgstr "Ieškoma apibrėžimo…"
